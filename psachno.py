# python -m pip install pygame --upgrade git+https://github.com/karulis/pybluez.git --upgrade git+https://github.com/HeyITGuyFixIt/wireless-radar.git@patch-1
import platform
# python -m pip install --upgrade git+https://github.com/HeyITGuyFixIt/wireless-radar.git@patch-1
import wirelessradar
from pygame.locals import *
import pygame  # python -m pip install pygame
import bluetooth  # python -m pip install --upgrade git+https://github.com/karulis/pybluez.git
import sys
import os
import time
import tkinter as tk
import subprocess
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)))
import provoli
root = tk.Tk()
app = provoli.App(root, "Psachno Scanner", bg="green")
w = app.winfo_width()-200
h = app.winfo_height()-200
# creates embed frame for pygame window
embed = tk.Frame(app.content, width=w, height=h)
embed.grid(columnspan=(600), rowspan=500)  # Adds grid
embed.pack(side=tk.TOP)  # packs window to the left
buttonwin = tk.Frame(app.content, width=w, height=h)
buttonwin.pack(side=tk.TOP)
os.environ['SDL_WINDOWID'] = str(embed.winfo_id())
if platform.system == "Windows":
    os.environ['SDL_VIDEODRIVER'] = 'windib'
screen = pygame.display.set_mode((w, h))
screen.fill(pygame.Color(0, 0, 0))
pygame.display.init()
pygame.display.update()

wifi_nets = list(filter(None, subprocess.check_output(
    ["netsh", "wlan", "show", "network"]).decode("utf-8").split('\r\n')))
del wifi_nets[0]
del wifi_nets[1]
i = 1
print(wifi_nets[0])
while i < len(wifi_nets):
    print(wifi_nets[i])
    print(wifi_nets[i+1])
    print(wifi_nets[i+2])
    print(wifi_nets[i+3])
    i += 4


def draw():
    pygame.draw.circle(screen, (0, 255, 0), (250, 250), 125)
    pygame.display.update()
    root.update()


button1 = tk.Button(buttonwin, text='Draw', command=draw)
button1.pack(side=tk.BOTTOM, anchor=tk.SW)
app.update()

while True:
    pygame.display.update()
    app.update()
root.mainloop()
